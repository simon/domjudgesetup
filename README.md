# Setup DOMjudge machine

Setup/configure domjudge machine for coding competitions (or a course in programming algorithms)

[See documentation](https://www.domjudge.org/snapshot/manual/install-domserver.html)

[Github link](https://github.com/DOMjudge/domjudge.git)

# building domserver/judgehost

``` console
apt install autoconf automake bats python3-sphinx python3-sphinx-rtd-theme rst2pdf fontconfig python3-yaml
```

# install domserver

``` console
sudo apt install acl zip unzip mariadb-server apache2 \
      php php-fpm php-gd php-cli php-intl php-mbstring php-mysql \
      php-curl php-json php-xml php-zip composer ntp
```

# install judgehost(s)



## What it does...

`devbox` launches a standard Ubuntu LXC container and configures it as a web development system. It makes you feel 'at home' as it creates your personal account(including ssh-keys) inside the container.

Software currently installed:
- Ubuntu 20.04
- Apache
- PHP
- MariaDB
- Postgresql

After setup of a new devbox, you should be able to just ssh into it, mount the web-documentroot in the Nautilus filemanager, and use your local web browser to
render the webapp.

## Requirements
Make sure you have a fully functional LXD/LXC installation setup for non-privileged containers on your Ubuntu 18.04 or 20.04 desktop pc. You'll need to have `jq` installed on your PC:
```
$ sudo apt install jq
```

## Installation
Clone this repository;
``` console
$ git clone git@gitlab.science.ru.nl:bram/devbox.git
```
Install the required python packages:
``` console
$ cd devbox
$ sudo pip3 install -r requirements.txt
```

> If you don't have sudo super powers.. Install the python
> packages in a virtual environment. With something like this:
> ```
> python3 -m venv venv
> . venv/bin/activate
> pip install -r requirements.txt
> ```

Now that Ansible is installed, install some roles:
``` console
$ ansible-galaxy install -r requirements.yml
```

## Usage
Create a new development environment:
``` console
$ ./devbox www01
```
Login:
``` console
$ ssh www01
```

Update a development environment:
``` console
$ ./devbox www01
```

Listing:
``` console
$ lxc list
```
The output shows the private class ip address of each container. Use this ip address in your local browser.

Stopping:
``` console
$ lxc stop www01
```

Starting
``` console
$ lxc start www01
```

Deleting:
``` console
$ lxc delete www01
```

## LXD Installation
Install and configure `LXD` with:
``` console
$ sudo apt install lxd
$ lxd init
Would you like to use LXD clustering? (yes/no) [default=no]:
Do you want to configure a new storage pool? (yes/no) [default=yes]:
Name of the new storage pool [default=default]:
Name of the storage backend to use (lvm, zfs, ceph, btrfs, dir) [default=zfs]: dir
Would you like to connect to a MAAS server? (yes/no) [default=no]:
Would you like to create a new local network bridge? (yes/no) [default=yes]:
What should the new bridge be called? [default=lxdbr0]:
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: none
Would you like LXD to be available over the network? (yes/no) [default=no]:
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:
```

Make sure you are member of the `lxd` unix group. You should be able to run `lxc list` without any error messages.

### LXC problems on Ubuntu 18.04
#### `Error: Get http://unix.socket/1.0`
On ubuntu 18 machines, I got the following error:
``` console
$  lxc launch ubuntu:18.04
Error: Get http://unix.socket/1.0: dial unix /var/lib/lxd/unix.socket: connect: permission denied
```
Install python packages:
``` console
$ sudo pip3 install -r requirements.txt
```
**FIX**
```
# chgrp lxd /var/lib/lxd
# chmod g+rwx /var/lib/lxd/
```
#### `Error: Failed to run...`
```
Error: Failed to run: /usr/lib/lxd/lxd forkstart test /var/lib/lxd/containers /var/log/lxd/test/lxc.conf:
Try `lxc info --show-log test` for more info
```
**FIX**
Not sure, but a reinstall seemed to fix it...:
``` console
# apt purge lxd lxcfs
```
And install:
```
# apt install lxd lxd-client
```
Now init lxd again:
```
# lxd init
```

#### Help! No IP addresses for my containers
After a reboot of your desktop pc, the containers might not get an ip address. This can be solved by restarting LXD:
``` console
$ sudo systemctl restart lxd
```

